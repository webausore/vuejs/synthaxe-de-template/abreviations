Vue.component('app-nombre', {
	template: '<span>{{nombre}}</span>',
	props: ['nombre'],
});

const app = new Vue({
	el: '#app',
	data: {
		msg:
			'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas',
		rawHtml: '<span style="color: red">sit aspernatur aut odit aut fugit</span>',
		nombre: Math.floor(Math.random() * 101), // nombre aléatoire de 0 a 100
		btnActive: true,
	},
	methods: {
		randomInt() {
			this.nombre = Math.floor(Math.random() * 101);
		},
	},
	computed: {},
});
