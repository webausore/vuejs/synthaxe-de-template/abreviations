# Vue JS Synthaxe de template - Abréviation

## Exercice

Préview : https://vuejs-synthaxe-de-template-abreviation.netlify.app/

Refaire [cet exercice](https://gitlab.com/webausore/vuejs/synthaxe-de-template/interpolations) en utilisant cette fois-ci les abréviations vu dans la partie Abréviation.
